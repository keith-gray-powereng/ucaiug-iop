import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Removes all destination address elements (e.g. MAC and IP)  in SMV Elements .

    ED2.1 schema  defines the datSet attribute as optional since they are "P" types.
    However, IEC 61850-8-1 clause 25.3.2 mandates all of the atttributes being present.

    The produced file should NOT verify and the GOOSE publishers should not be
    able to publish.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    smv_comms = scl_tree.findall("//scl:SMV", scl_namespaces)
    for smv_el in smv_comms:
        address = smv_el.find(".//scl:Address", scl_namespaces)
        if address is not None:
            pTypes = smv_el.findall(".//scl:P", scl_namespaces)
            for pType in pTypes[::-1]:
                typeValue = pType.get("type")
                if typeValue is not None:
                    if (
                        typeValue == "MAC-Address"
                        or typeValue == "IP"
                        or typeValue == "IPv6"
                    ):
                        logger.debug(
                            "Removing %s element from SMV with ldInst=%s and cbName=%s",
                            typeValue,
                            smv_el.get("ldInst"),
                            smv_el.get("cbName"),
                        )
                        parent = pType.getparent()
                        parent.remove(pType)
    return scl_tree
