import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Sets the datSet attribute value all GSEControl blocks to a non-existent
    (fake) dataSet.


    The produced file should potentially verify against schema, but may not be
    importable and may not and the GOOSE publishers should not verify.


    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    ieds = scl_tree.findall("scl:IED", scl_namespaces)
    for ied in ieds:
        iedName = ied.get("name")
        logger.debug("\tProcessing IED: %s", iedName)
        logical_devices = ied.findall(".//scl:LDevice", namespaces=scl_namespaces)
        for logical_device in logical_devices:
            ldInst = logical_device.get("inst")
            LN0 = logical_device.find(".//scl:LN0", namespaces=scl_namespaces)

            gsecbs = LN0.findall(".//scl:GSEControl", namespaces=scl_namespaces)
            for gsecb in gsecbs:
                updateGCB(ied.get("name"), ldInst, "LLN0", gsecb)

    return scl_tree


def updateGCB(iedName, ldInst, lnName, gcb):
    logger = logging.getLogger(__name__)

    datSetName = gcb.get("datSet")
    if datSetName is not None:
        gcb.attrib["datSet"] = "mxyzptlk_non_existent"
        logger.debug(
            "\tdatSet changed from %s to mxyzptlk_non_existent for %s%s/%s.%s",
            datSetName,
            iedName,
            ldInst,
            lnName,
            gcb.get("name"),
        )
    else:
        logger.error(
            "\tdatSet attribute not found in: %s%s/%s.%s",
            iedName,
            ldInst,
            lnName,
            gcb.get("name"),
        )
