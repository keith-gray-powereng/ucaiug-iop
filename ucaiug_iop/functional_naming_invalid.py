import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Set the ldName attribute of LDevice to an invalid value, if ldName is
    supported by the IED.

    Iterate through the IEDs and determine whether each IED supports Functional Naming.
    This is done by inspecting the Services->ConfLdName element.
    If the IED does support Functional Naming, set the ldName attribute on each LDevice
    to an invalid value. A value with a space (" ") was chosen for this test.

    The ldName must be unique within the subnetwork. This function simplifies that
    requirement by making all of the ldNames unique within the entire substation.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running functional_naming_invalid.execute()")
    logical_devices = scl_tree.xpath(
        "/scl:SCL/scl:IED/scl:AccessPoint/scl:Server/scl:LDevice",
        namespaces=scl_namespaces,
    )
    ldname_index = 0
    for logical_device in logical_devices:
        ied = logical_device.getparent().getparent().getparent()
        services = ied.find("scl:Services", namespaces=scl_namespaces)
        if services.find("scl:ConfLdName", namespaces=scl_namespaces) is not None:
            logger.debug(
                "Setting LDName on %s %s",
                ied.get("name"),
                logical_device.get("inst"),
            )
            logger.debug("\tBefore: %s", logical_device.attrib)
            logical_device.attrib["ldName"] = f"Unique_ldName {ldname_index}"
            ldname_index += 1
            logger.debug("\tAfter: %s", logical_device.attrib)
        else:
            logger.warning("Functional Naming not supported by %s", ied.get("name"))
    return scl_tree
