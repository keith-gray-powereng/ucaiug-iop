import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Set the P->MAC Address attribute of SMV to a unicast address.

    Find all of the <P type="MAC Address"> elements and change its value from the
    current setting, which is a multicast address, to a unicast address.
    It does this by changing the least significant bit of the most significant octet
    to a zero.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running sv_unicast.execute()")
    smv_macs = scl_tree.xpath(
        "scl:Communication/scl:SubNetwork/scl:ConnectedAP/"
        "scl:SMV/scl:Address/scl:P[@type='MAC-Address']",
        namespaces=scl_namespaces,
    )
    for smv_mac in smv_macs:
        connected_ap = smv_mac.getparent().getparent().getparent()
        gse = smv_mac.getparent().getparent()
        logger.debug("%s contains a SMV element", connected_ap.get("iedName"))
        mac_address = smv_mac.text
        first_octet, *rest = mac_address.split("-")
        first_octet = f"{(int(first_octet) & 0b11111110):02X}"
        new_mac_address = "-".join([first_octet] + rest)
        smv_mac.text = new_mac_address
        logger.debug(
            "Changed SMV (ldInst=%s, cbName=%s) MAC Address from %s to %s",
            gse.get("ldInst"),
            gse.get("cbName"),
            mac_address,
            new_mac_address,
        )
    return scl_tree
