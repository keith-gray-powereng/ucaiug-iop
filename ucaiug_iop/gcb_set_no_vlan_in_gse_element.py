import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Removes the vlan attribute value all Communication GSE Elements

    The produced file should NOT verify and the GOOSE publishers should not be
    able to publish (See Table 27).

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    gse_comms = scl_tree.findall("//scl:GSE", scl_namespaces)
    for gse_el in gse_comms:
        address = gse_el.find(".//scl:Address", scl_namespaces)
        if address is not None:
            pTypes = gse_el.findall(".//scl:P", scl_namespaces)
            for pType in pTypes[::-1]:
                typeValue = pType.get("type")
                if typeValue == "VLAN-ID":
                    logger.debug(
                        "Removing VLAN-ID element for GSE with ldInst=%s and cbName=%s",
                        gse_el.get("ldInst"),
                        gse_el.get("cbName"),
                    )
                    parent = pType.getparent()
                    parent.remove(pType)
    return scl_tree
