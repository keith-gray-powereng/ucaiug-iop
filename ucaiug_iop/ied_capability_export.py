import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """
    Determines the Reporting, GOOSE, and SV Capabilities of each IED in the SCD

    Exports to a CSV file

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: IED Capabilities
    :rtype: nested lists of the table containing the IED capabilities

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running functional_naming_invalid.execute()")
    retList = []
    header = [
        "IEDName",
        "Report Consumer",
        "Report Producer",
        "GOOSE Subscriber",
        "GOOSE Publisher",
        "SMV Subscriber",
        "SMV Publisher",
    ]
    retList.append(header)
    ieds = scl_tree.xpath("/scl:SCL/scl:IED", namespaces=scl_namespaces)

    for ied in ieds:
        ied_name = ied.get("name")
        logger.debug("IED Name: %s", ied_name)

        ###########################################
        # get the client and subscriber information
        ###########################################
        report_consumer = ied.xpath(
            ".//scl:ClientServices[@bufReport='true'] | "
            ".//scl:ClientServices[@unbufReport='true']",
            namespaces=scl_namespaces,
        )
        logger.debug("Report Consumer Elements: %s", report_consumer)
        goose_subscriber = ied.xpath(
            ".//scl:ClientServices[@goose='true']", namespaces=scl_namespaces
        )
        logger.debug("GOOSE Subscriber Elements: %s", goose_subscriber)
        sv_subscriber = ied.xpath(
            ".//scl:ClientServices[@sv='true']", namespaces=scl_namespaces
        )
        logger.debug("SV Subscriber Elements: %s", goose_subscriber)

        ###########################################
        # get the server capability information
        ###########################################
        report_producer = ied.findall(
            ".//scl:ConfReportControl", namespaces=scl_namespaces
        )

        goose_publisher = ied.findall(".//scl:GOOSE", namespaces=scl_namespaces)

        sv_publisher = ied.findall(".//scl:SMVsc", namespaces=scl_namespaces)

        # now that we have all of the values, time to put in a format that can be
        #  written to a csv using a csv writer
        iedCapabilities = [
            ied_name,
            "RPT-C" if len(report_consumer) > 0 else "",
            "RPT-P" if len(report_producer) > 0 else "",
            "GOOSE-S" if len(goose_subscriber) > 0 else "",
            "GOOSE-P" if len(goose_publisher) > 0 else "",
            "SMV-S" if len(sv_subscriber) > 0 else "",
            "SMV-P" if len(sv_publisher) > 0 else "",
        ]
        retList.append(iedCapabilities)

    return list(retList)
