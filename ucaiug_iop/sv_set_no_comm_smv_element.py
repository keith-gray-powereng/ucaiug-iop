import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Removes all SMV Elements in the Communication section in
    order to disable GOOSE Publications.


    The produced file should verify and the GOOSE publishers should not be
    able to publish.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    smv_comms = scl_tree.findall("//scl:SMV", scl_namespaces)
    for smv_el in smv_comms[::-1]:
        logger.debug(
            "Removing GSE element with ldInst=%s and cbName=%s "
            "from Communications Section",
            smv_el.get("ldInst"),
            smv_el.get("cbName"),
        )
        parent = smv_el.getparent()
        parent.remove(smv_el)

    return scl_tree
