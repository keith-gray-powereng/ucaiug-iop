import logging
from copy import deepcopy
from datetime import datetime
from importlib.metadata import version
from pathlib import Path
from tkinter import Tk
from tkinter.filedialog import askopenfilename

import click
from lxml import etree
from openpyxl import Workbook
from openpyxl.styles import Border
from openpyxl.styles import Side
from openpyxl.utils import get_column_letter

# from ucaiug_iop import rcb_dataset_null
from ucaiug_iop import functional_naming
from ucaiug_iop import functional_naming_all
from ucaiug_iop import functional_naming_duplicate
from ucaiug_iop import functional_naming_invalid
from ucaiug_iop import gcb_exceeds_lpdu_size
from ucaiug_iop import gcb_set_appid_empty
from ucaiug_iop import gcb_set_dataset_empty
from ucaiug_iop import gcb_set_dataset_non_existent
from ucaiug_iop import gcb_set_dataset_null
from ucaiug_iop import gcb_set_no_appid_in_gse_element
from ucaiug_iop import gcb_set_no_comm_gse_element
from ucaiug_iop import gcb_set_no_dstAddr_in_gse_element
from ucaiug_iop import gcb_set_no_vlan_in_gse_element
from ucaiug_iop import goose_unicast
from ucaiug_iop import ied_capability_export
from ucaiug_iop import may_ignore
from ucaiug_iop import must_understand
from ucaiug_iop import rcb_set_dataset_empty
from ucaiug_iop import rcb_set_dataset_non_existent
from ucaiug_iop import sv_set_dataset_empty
from ucaiug_iop import sv_set_dataset_non_existent
from ucaiug_iop import sv_set_dataset_null
from ucaiug_iop import sv_set_no_comm_smv_element
from ucaiug_iop import sv_set_no_dst_addr_in_smv_element
from ucaiug_iop import sv_set_no_vlan_in_smv_element
from ucaiug_iop import sv_set_num_of_adsu_0
from ucaiug_iop import sv_set_num_of_adsu_1m
from ucaiug_iop import sv_unicast
from ucaiug_iop import svcb_set_dataset_null
from ucaiug_iop.utils import configure_logging
from ucaiug_iop.utils import get_log_level
from ucaiug_iop.utils import write_scd


@click.group("ucaiug-iop", chain=True, invoke_without_command=True)
@click.option(
    "--debug/--no-debug",
    default=False,
    help="Use this flag to enable more verbose logging to the console.",
)
@click.option(
    "-f",
    "--scl_file_path",
    help=(
        "Provide the path to the 'Base' SCD file. If this path is not provided, "
        "you will be prompted to choose the SCD file from a dialog box."
    ),
    type=click.Path(exists=True),
)
@click.version_option(
    version=version("ucaiug_iop"), prog_name="ucaiug-iop"
)  # version=__version__
@click.pass_context
def cli(ctx, scl_file_path, debug):
    print("*" * 82)
    start_time = datetime.now()
    print(f"Application Started at: {start_time}")
    print("*" * 82)
    ctx.ensure_object(dict)
    log_level = get_log_level(debug)
    configure_logging(log_level)
    logger = logging.getLogger(__name__)
    if not scl_file_path:
        Tk().withdraw()
        scl_file_path = askopenfilename(
            title="Select 'Base' SCD File",
            filetypes=(
                ("SCD Files", "*.scd"),
                ("ICD Files", "*.icd"),
                ("SSD Files", "*.ssd"),
                ("CID Files", "*.cid"),
                ("IID Files", "*.iid"),
                ("SED Files", "*.sed"),
            ),
        )
        if not scl_file_path:
            logger.error("You must select the 'Base' SCD file.")
            return
    logger.info("Parsing SCL File: %s", scl_file_path)
    scl_file_path = Path(scl_file_path)
    scl_tree = etree.parse(
        str(scl_file_path), parser=etree.XMLParser(strip_cdata=False)
    )
    ctx.obj["scl-file-path"] = scl_file_path
    ctx.obj["scl-tree"] = scl_tree
    if ctx.invoked_subcommand is None:
        ctx.invoke(ied_capability_export_subcommand)
        ctx.invoke(gcb_exceeds_lpdu_size_subcommand)
        ctx.invoke(may_ignore_subcommand)
        ctx.invoke(functional_naming_subcommand)
        ctx.invoke(functional_naming_invalid_subcommand)
        ctx.invoke(functional_naming_all_subcommand)
        ctx.invoke(functional_naming_duplicate_subcommand)
        ctx.invoke(rcb_dataset_empty_subcommand)
        ctx.invoke(rcb_set_dataset_non_existent_subcommand)
        ctx.invoke(gse_remove_appid_subcommand)
        ctx.invoke(gse_remove_destination_address_subcommand)
        ctx.invoke(gcb_dataset_null_subcommand)
        ctx.invoke(gcb_dataset_empty_subcommand)
        ctx.invoke(gcb_appid_empty_subcommand)
        ctx.invoke(goose_unicast_subcommand)
        ctx.invoke(must_understand_subcommand)
        ctx.invoke(sv_unicast_subcommand)
        ctx.invoke(sv_dataset_null_subcommand)
        ctx.invoke(comm_remove_gse_subcommand)
        ctx.invoke(gcb_set_dataset_non_existent_subcommand)
        ctx.invoke(gcb_set_no_vlan_in_gse_subcommand)
        ctx.invoke(sv_set_dataset_empty_subcommand)
        ctx.invoke(sv_set_dataset_non_existent_subcommand)
        ctx.invoke(sv_set_dataset_null_subcommand)
        ctx.invoke(sv_set_no_comm_smv_element_subcommand)
        ctx.invoke(sv_set_no_dst_addr_smv_element_subcommand)
        ctx.invoke(sv_set_no_vlan_in_smv_element_subcommand)
        ctx.invoke(sv_set_num_of_adsu_0_subcommand)
        ctx.invoke(sv_set_num_of_adsu_1M_subcommand)

    @ctx.call_on_close
    def print_footer():
        print("*" * 82)
        end_time = datetime.now()
        print(f"Application Stopped at: {end_time}")
        print(f"Total Execution Time: {end_time - start_time}")
        print("*" * 82)


@cli.command("Functional-Naming")
@click.pass_context
def functional_naming_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running Functional Naming on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("functional_naming.scd"),
        functional_naming.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("Functional-Naming-All")
@click.pass_context
def functional_naming_all_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running Functional Naming All on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("functional_naming_all.scd"),
        functional_naming_all.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("Functional-Naming-Invalid")
@click.pass_context
def functional_naming_invalid_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running Functional Naming Invalid on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("functional_naming_invalid.scd"),
        functional_naming_invalid.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("Functional-Naming-Duplicate")
@click.pass_context
def functional_naming_duplicate_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running Functional Naming Duplicate on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("functional_naming_duplicate.scd"),
        functional_naming_duplicate.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("RCB-Dataset-Empty")
@click.pass_context
def rcb_dataset_empty_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running RCB Dataset Empty on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("rcb_dataset_empty.scd"),
        rcb_set_dataset_empty.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GSE-Remove-APPID")
@click.pass_context
def gse_remove_appid_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GSE Remove APPID on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("gse_remove_appid.scd"),
        gcb_set_no_appid_in_gse_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GSE-Remove-Destination-Address")
@click.pass_context
def gse_remove_destination_address_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running GSE Remove Destination Address on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("gse_remove_address_p_type.scd"),
        gcb_set_no_dstAddr_in_gse_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GCB-Dataset-Null")
@click.pass_context
def gcb_dataset_null_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GCB Dataset Null on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("gcb_dataset_null.scd"),
        gcb_set_dataset_null.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GCB-Dataset-Empty")
@click.pass_context
def gcb_dataset_empty_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GCB Dataset Empty on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("gcb_dataset_empty.scd"),
        gcb_set_dataset_empty.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GCB-APPID-Empty")
@click.pass_context
def gcb_appid_empty_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GCB APPID Empty on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("gcb_appid_empty.scd"),
        gcb_set_appid_empty.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GOOSE-Unicast")
@click.pass_context
def goose_unicast_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GOOSE Unicast on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("goose_unicast.scd"),
        goose_unicast.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-Unicast")
@click.pass_context
def sv_unicast_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running SV Unicast on %s", ctx.obj["scl-file-path"].name)
    write_scd(Path("sv_unicast.scd"), sv_unicast.execute(deepcopy(ctx.obj["scl-tree"])))


@cli.command("SV-Dataset-Null")
@click.pass_context
def sv_dataset_null_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running SV Dataset Null on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("sv_dataset_null.scd"),
        svcb_set_dataset_null.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("Comm-Remove-GSE")
@click.pass_context
def comm_remove_gse_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GSE Remove Comm on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("comm_remove_gse.scd"),
        gcb_set_no_comm_gse_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GSE-Remove-VLAN")
@click.pass_context
def gcb_set_no_vlan_in_gse_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running GSE Remove VLAN-ID on %s", ctx.obj["scl-file-path"].name)
    write_scd(
        Path("gse_remove_vlan_id.scd"),
        gcb_set_no_vlan_in_gse_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("GCB-SET-DATASET-NON-EXISTENT")
@click.pass_context
def gcb_set_dataset_non_existent_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running GCB Set dataset to non-existent dataset on %s",
        ctx.obj["scl-file-path"].name,
    )
    write_scd(
        Path("gcb_set_dataset_non_existent.scd"),
        gcb_set_dataset_non_existent.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("RCB-SET-DATASET-NON-EXISTENT")
@click.pass_context
def rcb_set_dataset_non_existent_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running RCB Set dataset to non-existent dataset on %s",
        ctx.obj["scl-file-path"].name,
    )
    write_scd(
        Path("rcb_set_dataset_non_existent.scd"),
        rcb_set_dataset_non_existent.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-DATASET-NULL")
@click.pass_context
def sv_set_dataset_null_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running SV remove dataset attribute on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("sv_set_dataset_null.scd"),
        sv_set_dataset_null.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-DATASET-EMPTY")
@click.pass_context
def sv_set_dataset_empty_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running SV dataset attribute empty on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("sv_set_dataset_empty.scd"),
        sv_set_dataset_empty.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-DATASET-NON-EXISTENT")
@click.pass_context
def sv_set_dataset_non_existent_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running SV  dataset attribute to non-existent on %s",
        ctx.obj["scl-file-path"].name,
    )
    write_scd(
        Path("sv_set_dataset_non_existent.scd"),
        sv_set_dataset_non_existent.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-NO-COMM")
@click.pass_context
def sv_set_no_comm_smv_element_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running removal of SMV Communication Elements on %s",
        ctx.obj["scl-file-path"].name,
    )
    write_scd(
        Path("sv_set_no_comm_smv_element.scd"),
        sv_set_no_comm_smv_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-NO-DSTADDR")
@click.pass_context
def sv_set_no_dst_addr_smv_element_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running removal of destination addresses in SMV on %s",
        ctx.obj["scl-file-path"].name,
    )
    write_scd(
        Path("sv_set_no_dstAddr_in_smv_element.scd"),
        sv_set_no_dst_addr_in_smv_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-NO-VLAN")
@click.pass_context
def sv_set_no_vlan_in_smv_element_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running removal of VLAN_ID in SMV on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("sv_set_no_vlan_in_smv_element.scd"),
        sv_set_no_vlan_in_smv_element.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-NUM-ADSU-0")
@click.pass_context
def sv_set_num_of_adsu_0_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running set nofASDU=0 in SV Controls on %s", ctx.obj["scl-file-path"].name
    )
    write_scd(
        Path("sv_set_num_of_adsu_0.scd"),
        sv_set_num_of_adsu_0.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("SV-SET-NUM-ADSU-1M")
@click.pass_context
def sv_set_num_of_adsu_1M_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info(
        "Running set nofASDU=1000000 in SV Controls on %s",
        ctx.obj["scl-file-path"].name,
    )
    write_scd(
        Path("sv_set_num_of_adsu_1M.scd"),
        sv_set_num_of_adsu_1m.execute(deepcopy(ctx.obj["scl-tree"])),
    )


@cli.command("MAY-IGNORE")
@click.pass_context
def may_ignore_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running may ignore manipulation %s", ctx.obj["scl-file-path"].name)
    try:
        write_scd(
            Path("may_ignore.scd"),
            may_ignore.execute(deepcopy(ctx.obj["scl-tree"])),
        )
    except ValueError as e:
        logger.error("may_ignore module exception: %s", e)


@cli.command("GCB-DATASET-EXCEEDS-L2-LPDU")
@click.pass_context
def gcb_exceeds_lpdu_size_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running gcb l2 exceeds LPDU size %s", ctx.obj["scl-file-path"].name)
    try:
        write_scd(
            Path("gcb_exceeds_lpdu_size.scd"),
            gcb_exceeds_lpdu_size.execute(deepcopy(ctx.obj["scl-tree"])),
        )
    except ValueError as e:
        logger.error("gcb_exceeds_lpdu_size module exception: %s", e)


@cli.command("IED-CAPABILITY")
@click.pass_context
def ied_capability_export_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running ied capability export %s", ctx.obj["scl-file-path"].name)
    try:
        retRows = ied_capability_export.execute(deepcopy(ctx.obj["scl-tree"]))
        wb = Workbook()
        ws = wb.active
        ws.title = "IED Capabilities"
        header_cell_border = Border(
            left=Side(border_style=None, style="thick"),
            right=Side(border_style=None, style="thick"),
            top=Side(border_style=None, style="thick"),
            bottom=Side(border_style=None, style="thick"),
        )
        data_cell_border = Border(
            left=Side(border_style=None, style="thin"),
            right=Side(border_style=None, style="thin"),
            top=Side(border_style=None, style="thin"),
            bottom=Side(border_style=None, style="thin"),
        )
        for row_index, row in enumerate(retRows, 1):
            for col_index, cell in enumerate(row, 1):
                ws.cell(row_index, col_index).value = cell
                if row_index == 1:
                    ws.cell(row_index, col_index).border = header_cell_border
                else:
                    ws.cell(row_index, col_index).border = data_cell_border
        for column_cells in ws.columns:
            length = max(len(cell.value or "") for cell in column_cells)
            ws.column_dimensions[get_column_letter(column_cells[0].column)].width = (
                length * 1.2
            )
        ws.auto_filter.ref = ws.dimensions
        wb.save(filename="ied_capabilities.xlsx")
        logger.info("IED Capability Report written to ied_capabilities.xlsx.")
    except Exception as e:
        logger.error("ied_capability module exception: %s", e)


@cli.command("MUST-UNDERSTAND-R-GOOSE")
@click.pass_context
def must_understand_subcommand(ctx):
    logger = logging.getLogger(__name__)
    print("-" * 82)
    logger.info("Running mustunderstand R-GOOSE %s", ctx.obj["scl-file-path"].name)
    try:
        write_scd(
            Path("must_understand.scd"),
            must_understand.execute(deepcopy(ctx.obj["scl-tree"])),
        )
    except ValueError as e:
        logger.error("must_understand module exception: %s", e)


if __name__ == "__main__":  # pragma: no cover
    cli()
