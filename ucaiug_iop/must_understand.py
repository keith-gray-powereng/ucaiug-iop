import logging

from lxml import etree
from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Produces SCL with mustUnderstand included.

    ED2.1 61850-6 defines a set of rules named must understand and may ignore.
    This module generates a SCD for testing the mustunderstand.

    The process is to use an IED whose name is "fake".  If this IED
    is not present, that fact will be logged and no SCD will be returned.

    gcb1 will be used to enroll all of the IEDs as subscribers as "IEDName" elements.
    After the IEDNames are added , an Element will be added whose.  The R-GOOSE protocol
    is specified with "mustunderstand = true"

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)

    ####################################
    # determine if "fake" IED is present
    ####################################
    fake_ied = scl_tree.find("//scl:IED[@name='fake']", scl_namespaces)
    if fake_ied is None:
        logger.error("fake IED not found, no SCD will be generated")
    else:
        try:
            perform_may_ignore(scl_tree, fake_ied)
        except Exception as e:
            logger.error("Exception: $s", e)
    return scl_tree


def perform_may_ignore(scl_tree, fake_ied):
    logger = logging.getLogger(__name__)

    try:
        fake_gse_control = fake_ied.find(
            ".//scl:GSEControl[@name='gcb1']", scl_namespaces
        )

        ieds = scl_tree.findall("scl:IED", scl_namespaces)

        for ied in ieds:
            name = ied.get("name")
            if name != "fake":
                etree.SubElement(fake_gse_control, "IEDName").text = name

        protocol_element = etree.SubElement(fake_gse_control, "Protocol")
        protocol_element.set("mustUnderstand", "true")
        protocol_element.text = "R-GOOSE"

    except Exception as e:
        logger.error("Exception: $s", e)
