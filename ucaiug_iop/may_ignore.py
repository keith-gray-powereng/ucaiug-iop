import logging

from lxml import etree
from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """removes the datSet attribute all GSEControl blocks.

    ED2.1 61850-6 defines a set of rules named must understand and may ignore.
    This module generates a SCD for testing the may ignore.

    The process is to use an IED whose name is "fake".  If this IED
    is not present, that fact will be logged and no SCD will be returned.

    The SCL version will be changed to "2021"

    If it is present, a list of all IEDs/Names will be retrieved and added to
    the rcb in the "fake" IED as "ClientLN" elements within the rcb/RptEnable
    The max attribute will also be set to the number of IEDs that are being added
    as the ClientLN. After the ClientLNs are added , an Element will be added whose
    tag is "mayIgnore" with a random value.


    gcb1 will be used to enroll all of the IEDs as subscribers as "IEDName" elements.
    After the IEDNames are added , an Element will be added whose
    tag is "mayIgnore" with a random value.

    Both methods will not include "fake"/

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)

    """
    determine if "fake" IED is present
    """
    root = scl_tree.getroot()
    isSCL = root.tag
    substring = "SCL"
    if substring in isSCL:
        fakeIED = scl_tree.find("//scl:IED[@name='fake']", scl_namespaces)
        if fakeIED is None:
            logger.error("fake IED not found, no SCD will be generated")
        else:
            try:
                performMayIgnore(scl_tree)
            except Exception as e:
                logger.error("Exception: $s", e)
    return scl_tree


def performMayIgnore(scl_tree):
    logger = logging.getLogger(__name__)

    scl_element = scl_tree.getroot()
    scl_element.attrib["version"] = "2021"

    fakeIED = scl_tree.find("//scl:IED[@name='fake']", scl_namespaces)
    if fakeIED is None:
        raise ValueError("fake IED not found, no SCD will be generated")

    fakeIED.attrib["originalSclVersion"] = "2021"
    try:

        fakeRCBEna = fakeIED.find(
            ".//scl:ReportControl[@name='rcb1']/scl:RptEnabled", scl_namespaces
        )
        fakeGSEControl = fakeIED.find(".//scl:GSEControl[@name='gcb1']", scl_namespaces)

        ieds = scl_tree.findall("scl:IED", scl_namespaces)

        numIEDs = len(ieds)

        fakeRCBEna.attrib["max"] = str(numIEDs)

        etree.SubElement(fakeRCBEna, "mayignore").text = "may ignore value"

        for ied in ieds:
            name = ied.get("name")
            if name != "fake":
                clientLN = etree.SubElement(fakeRCBEna, "ClientLN")
                clientLN.set("iedName", name)
                clientLN.set("lnClass", "LLN0")
                clientLN.set("lnInst", "")
                clientLN.set("ldInst", "LD0")
                lds = ied.findall(".//scl:LDevice", scl_namespaces)

                numOfLds = len(lds)
                if numOfLds > 0:
                    element = lds[0]
                    ldinst = element.get("inst")
                    clientLN.set("ldInst", ldinst)

                etree.SubElement(fakeGSEControl, "IEDName").text = name

        etree.SubElement(fakeGSEControl, "mayignore").text = "may ignore value"
    except Exception as e:
        logger.error("Exception: $s", e)
