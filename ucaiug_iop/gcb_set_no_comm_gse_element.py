import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Removes all GSE Elements in the Communication section in
    order to disable GOOSE Publications.

    ED2.1 schema  defines the datSet attribute as optional

    The produced file should verify and the GOOSE publishers should not be
    able to publish.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    gse_comms = scl_tree.findall("//scl:GSE", scl_namespaces)
    for gse_el in gse_comms[::-1]:
        logger.debug(
            "Removing GSE element with ldInst=%s and cbName=%s "
            "from Communications Section",
            gse_el.get("ldInst"),
            gse_el.get("cbName"),
        )
        parent = gse_el.getparent()
        parent.remove(gse_el)

    return scl_tree
