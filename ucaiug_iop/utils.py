import logging
import sys
from logging.config import dictConfig
from pathlib import Path

from yaml import CSafeLoader
from yaml import load

scl_namespaces = {
    "xs": "http://www.w3.org/2001/XMLSchema",
    "scl": "http://www.iec.ch/61850/2003/SCL",
}


def configure_logging(log_level):
    config_file = str(
        Path(getattr(sys, "_MEIPASS", Path(__file__).parent))
        / Path("resources")
        / Path("logging-config.yaml")
    )
    with open(config_file, "rt") as f:
        config = load(f, Loader=CSafeLoader)
        config["handlers"]["console"]["level"] = log_level
        dictConfig(config)
    return


def get_log_level(verbose: bool) -> int:
    if verbose:
        return logging.DEBUG
    else:
        return logging.INFO


def write_scd(scd_path: Path, scl_tree):
    scl_tree.write(
        str(scd_path),
        pretty_print=True,
        encoding="UTF-8",
        xml_declaration=True,
    )
