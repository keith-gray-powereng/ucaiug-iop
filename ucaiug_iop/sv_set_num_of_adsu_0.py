import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Removes the number of ASDU attribute to a value of 0 for all SVControl blocks.

    The produced file should verify and the GOOSE publishers should not be
    able to publish.  Subscribers may issue warnings

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    ieds = scl_tree.findall("scl:IED", scl_namespaces)
    for ied in ieds:
        iedName = ied.get("name")
        logger.debug("\tProcessing IED: %s", iedName)
        logical_devices = ied.findall(".//scl:LDevice", namespaces=scl_namespaces)
        for logical_device in logical_devices:
            ldInst = logical_device.get("inst")
            LN0 = logical_device.find(".//scl:LN0", namespaces=scl_namespaces)
            if LN0 is not None:
                svcbs = LN0.findall(
                    ".//scl:SampledValueControl", namespaces=scl_namespaces
                )
                for svcb in svcbs:
                    updateSVCB(ied.get("name"), ldInst, "LLN0", svcb)
            else:
                logger.error(
                    "\tError: No LNN0 found in ied:%s ldInst:%s", iedName, ldInst
                )

    return scl_tree


def updateSVCB(iedName, ldInst, lnName, svcb):
    logger = logging.getLogger(__name__)

    nofASDU = svcb.get("nofASDU")
    if nofASDU is not None:
        svcb.attrib["nofASDU"] = "0"
        logger.debug(
            "\tnofASDU changed from %s to 0 for %s%s/%s.%s",
            nofASDU,
            iedName,
            ldInst,
            lnName,
            svcb.get("name"),
        )
    else:
        logger.error(
            "\tnofASDU attribute not found in: %s%s/%s.%s",
            iedName,
            ldInst,
            lnName,
            svcb.get("name"),
        )
