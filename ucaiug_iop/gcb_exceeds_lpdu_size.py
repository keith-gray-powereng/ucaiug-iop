import logging

from lxml import etree
from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """removes the datSet attribute all GSEControl blocks.

    The module subscribes all IEDs to a GSEControl block
    whose datSet is larger than can fit in a L2-GOOSE PDU.

    The process is to use an IED whose name is "fake". If this IED
    is not present, that fact will be logged and no SCD will be returned.

    This requires a specific "fake" IED to be in the SCD.

    Subscription will not include "fake"

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)

    """
    determine if "fake" IED is present
    """
    root = scl_tree.getroot()
    isSCL = root.tag
    substring = "SCL"
    if substring in isSCL:
        fakeIED = scl_tree.find("//scl:IED[@name='fake']", scl_namespaces)
        if fakeIED is None:
            logger.error("fake IED not found, no SCD will be generated")
            scl_tree = []
            raise ValueError("fake IED not found, no SCD will be generated")
        else:
            try:
                gse_control = fakeIED.find(".//scl:GSEControl[@name='gcb2']", scl_namespaces)
                gse_control.attrib['datSet'] = "dataset2"
                perform_goose_subscription(scl_tree)
            except Exception as e:
                logger.error("Exception: %s", e)
                scl_tree = []
    else:
        scl_tree = []
    return scl_tree


def perform_goose_subscription(scl_tree):
    logger = logging.getLogger(__name__)

    fakeIED = scl_tree.find("//scl:IED[@name='fake']", scl_namespaces)
    try:

        fakeGSEControl = fakeIED.find(".//scl:GSEControl[@name='gcb2']", scl_namespaces)

        ieds = scl_tree.findall("scl:IED", scl_namespaces)

        for ied in ieds:
            name = ied.get("name")
            if name != "fake":
                etree.SubElement(fakeGSEControl, "IEDName").text = name

    except Exception as e:
        logger.error("Exception: $s", e)
        scl_tree = []
