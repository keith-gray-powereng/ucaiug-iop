import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Removes all destination address elements (e.g. MAC and IP)  in GSE Elements .

    ED2.1 schema  defines the datSet attribute as optional since they are "P" types.
    However, IEC 61850-8-1 clause 25.3.2 mandates all of the atttributes being present.

    The produced file should NOT verify and the GOOSE publishers should not be
    able to publish.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    gse_comms = scl_tree.findall("//scl:GSE", scl_namespaces)
    for gse_el in gse_comms:
        address = gse_el.find(".//scl:Address", scl_namespaces)
        if address is not None:
            pTypes = gse_el.findall(".//scl:P", scl_namespaces)
            for pType in pTypes[::-1]:
                typeValue = pType.get("type")
                if typeValue is not None:
                    if (
                        typeValue == "MAC-Address"
                        or typeValue == "IP"
                        or typeValue == "IPv6"
                    ):
                        logger.debug(
                            "Removing %s element from GSE with ldInst=%s and cbName=%s",
                            typeValue,
                            gse_el.get("ldInst"),
                            gse_el.get("cbName"),
                        )
                        parent = pType.getparent()
                        parent.remove(pType)
    return scl_tree
