import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """removes the datSet attribute all report control blocks.

    ED2.1 schema  defines the datSet attribute as optional;.

    The produced file should verify and the Report Clients should not be
    able to enable the control block

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """

    logger = logging.getLogger(__name__)
    logger.debug("Running %s", __file__)
    ieds = scl_tree.findall("scl:IED", scl_namespaces)
    for ied in ieds:
        iedName = ied.get("name")
        logger.debug("\tProcessing IED: %s", iedName)
        logical_devices = ied.findall(".//scl:LDevice", namespaces=scl_namespaces)
        for logical_device in logical_devices:
            ldInst = logical_device.get("inst")

            LN0 = logical_device.find(".//scl:LN0", namespaces=scl_namespaces)
            if LN0 is not None:
                rcbs = LN0.findall(".//scl:ReportControl", namespaces=scl_namespaces)
                logger.debug(
                    "\tConverting report control blocks found in : %s%s/%s",
                    iedName,
                    ldInst,
                    "LLNO",
                )
                for rcb in rcbs:
                    updateRCB(ied.get("name"), ldInst, "LLN0", rcb)
            else:
                logger.error(
                    "\tError: No LNN0 found in ied:%s ldInst:%s", iedName, ldInst
                )

            logicalNodes = logical_device.findall(
                ".//scl:LN", namespaces=scl_namespaces
            )
            for logicalNode in logicalNodes:
                prefix = ""
                inst = ""

                prefix = logicalNode.get("prefix")

                inst = logicalNode.get("inst")
                if inst is None:
                    logger.error(
                        "\tLogical Node instance is missing mandatory attribute inst "
                        "in ied: %s ldInst:%s",
                        iedName,
                        ldInst,
                    )

                lnClass = logicalNode.get("lnClass")
                rcbs = []
                if lnClass is not None:
                    logicalNodeName = (
                        str(prefix) + logicalNode.get("lnClass") + str(inst)
                    )
                    rcbs = logicalNode.findall(
                        ".//scl:ReportControl", namespaces=scl_namespaces
                    )
                logger.debug(
                    "\tConverting report control blocks found in : %s%s/%s",
                    iedName,
                    ldInst,
                    logicalNodeName,
                )
                for rcb in rcbs:
                    updateRCB(ied.get("name"), ldInst, logicalNodeName, rcb)
    return scl_tree


def updateRCB(iedName, ldInst, lnName, rcb):
    logger = logging.getLogger(__name__)
    fc = "RP"
    isBufferred = "false"
    isBufferred = rcb.get("buffered")
    if isBufferred == "true":
        fc = "BR"

    datSetName = rcb.get("datSet")
    if datSetName is not None:
        rcb.attrib["datSet"] = "mxyzptlk_non_existent"
        logger.debug(
            "\tdatSet changed from %s to non-existent for %s%s/%s.%s",
            datSetName,
            iedName,
            ldInst,
            lnName,
            rcb.get("name"),
        )
