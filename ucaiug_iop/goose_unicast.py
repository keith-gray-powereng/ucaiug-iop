import logging

from lxml.etree import _ElementTree

from ucaiug_iop.utils import scl_namespaces


def execute(scl_tree: _ElementTree) -> _ElementTree:
    """Set the P->MAC Address attribute of GSE to a unicast address.

    Find all of the <P type="MAC Address"> elements and change its value from the
    current setting, which is a multicast address, to a unicast address.
    It does this by changing the least significant bit of the most significant octet
    to a zero.

    :param scl_tree _ElementTree: The original SCD file to be modified for this test
    :returns: Updated SCL file
    :rtype: _ElementTree

    """
    logger = logging.getLogger(__name__)
    logger.debug("Running goose_unicast.execute()")
    goose_macs = scl_tree.xpath(
        "scl:Communication/scl:SubNetwork/scl:ConnectedAP/"
        "scl:GSE/scl:Address/scl:P[@type='MAC-Address']",
        namespaces=scl_namespaces,
    )
    for goose_mac in goose_macs:
        connected_ap = goose_mac.getparent().getparent().getparent()
        gse = goose_mac.getparent().getparent()
        logger.debug("%s contains a GSE element", connected_ap.get("iedName"))
        mac_address = goose_mac.text
        first_octet, *rest = mac_address.split("-")
        first_octet = f"{(int(first_octet) & 0b11111110):02X}"
        new_mac_address = "-".join([first_octet] + rest)
        goose_mac.text = new_mac_address
        logger.debug(
            "Changed GSE (ldInst=%s, cbName=%s) MAC Address from %s to %s",
            gse.get("ldInst"),
            gse.get("cbName"),
            mac_address,
            new_mac_address,
        )
    return scl_tree
