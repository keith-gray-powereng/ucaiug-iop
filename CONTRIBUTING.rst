.. highlight:: shell

============
Contributing
============

.. important::

   All of the commands below must be run from the top level directory of the repository.
   This directory contains pyproject.toml.

Creating a Python Environment
=============================

It is highly recommended that you create a python environment for this application.
A dedicated python environment will allow you to manage dependencies and their
versions specific to this project, without interferring with other python
projects or installations you may have on your PC.

Using conda to create an environment
------------------------------------

The conda distribution of Python includes commands to management environments.
There are other ways to do this but conda is a known-good way.

The command below creates a new python 3.9 environment. You should replace
"<name-of-environment>" with a name that will allow you to identify this
environment. One suggestion would be "xsd-post-processor".

.. code-block:: batch

    > conda create -n <name-of-environment> python=3.9
    
Activating the Python Environment
=================================

In order to run the application in development, the python environment must be activated first.

.. code-block:: batch

   > conda activate <name-of-environment>


Managing Dependencies
=====================

This application uses `poetry <https://python-poetry.org/>`_ to manage dependencies.
Full documentation for poetry can be found at their `documentation <https://python-poetry.org/docs/>`_
site.

Installing Dependencies
-----------------------

The *install* command can be used to install all of the existing dependencies.
Run this command from the top-level directory of the project, which is the one
that contains *pyproject.toml*.

.. code-block:: batch

    > poetry install

Adding a Dependency
-------------------

The *add* command can be used to add a dependency. Replace *dependency_name* with
the name of the desired dependency, such as *openpyxl*.

.. code-block:: batch

    > poetry add dependency_name

To add a dependency that is only used during development, and not during runtime,
use the *-D* option to the *add* command.

.. code-block:: batch

    > poetry add -D dependency_name

Removing a Dependency
---------------------

If you added a dependency and then decide you no longer need it,
use the *remove* command to remove it from the project.


.. code-block:: batch

    > poetry remove dependency_name

Source Code Formatting
======================

All python source code (.py) files in the project, including tests, must be formatted
using the *black* formatter. *black* is installed as a development dependency and
can be run on your development laptop. Many text editors and IDEs can be configured
to run a code formatter when a file is saved. The following command can also be run manually.
It assumes the python environment is activated.

.. code-block:: batch

   > black --exclude resources xsd_post_processor

Running Tests
=============

This application uses pytest as the test runner.
Run the following command from the top-level directory
to execute the test suite

.. code-block:: batch

    > python -m pytest tests

If you want to see the log messages printed in the terminal
when the tests run, add the '-s' option.

.. code-block:: batch

    > python -m pytest -s tests

Filing Issues/Feature Requests
==============================

Issues and feature requests can be submitted on the Gitlab project 
`Issues page <https://gitlab.com/keith-gray-powereng/61850-80-5-xsd-post-processor/-/issues>`_.

Merge Requests
==============

Merge Requests are welcome. Fork the project on 
`Gitlab <https://gitlab.com/keith-gray-powereng/61850-80-5-xsd-post-processor/-/issues>`_,
make the changes, and submit a MR.
