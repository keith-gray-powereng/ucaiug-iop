from importlib.metadata import version
from pathlib import Path

from click.testing import CliRunner

from ucaiug_iop.ucaiug_iop import cli


def test_all_commands_at_once():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "Functional-Naming",
                "Functional-Naming-All",
                "Functional-Naming-Invalid",
                "Functional-Naming-Duplicate",
                "GOOSE-Unicast",
                "SV-Unicast",
                "SV-Dataset-Null",
                "RCB-Dataset-Empty",
                "GCB-Dataset-Null",
                "GCB-Dataset-Empty",
                "GCB-APPID-Empty",
                "GSE-Remove-APPID",
                "GSE-Remove-Destination-Address",
                "Comm-Remove-GSE",
            ],
        )
        assert result.exit_code == 0, result.output


def test_functional_naming():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "Functional-Naming"
            ]
        )
        assert result.exit_code == 0, result.output


def test_functional_naming_all():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "Functional-Naming-All"
            ]
        )
        assert result.exit_code == 0, result.output


def test_functional_naming_invalid():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "Functional-Naming-Invalid"
            ]
        )
        assert result.exit_code == 0, result.output


def test_functional_naming_duplicate():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "Functional-Naming-Duplicate"
            ]
        )
        assert result.exit_code == 0, result.output


def test_goose_unicast():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GOOSE-Unicast"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_unicast():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-Unicast"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_dataset_null():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-Dataset-Null"
            ]
        )
        assert result.exit_code == 0, result.output


def test_rcb_dataset_empty():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "RCB-Dataset-Empty"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gcb_dataset_null():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GCB-Dataset-Null"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gcb_dataset_empty():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GCB-Dataset-Empty"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gcb_appid_null():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GCB-APPID-Empty"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gcb_exceeds_lpdu_size():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/2021-08-27-iop-2007B-V0-R001.scd")),
                "GCB-DATASET-EXCEEDS-L2-LPDU"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gcb_set_dataset_non_existent():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GCB-SET-DATASET-NON-EXISTENT"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gcb_set_no_vlan_in_gse_element():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GSE-Remove-VLAN"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gse_remove_appid():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GSE-Remove-APPID"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gse_remove_comm():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "Comm-Remove-GSE"
            ]
        )
        assert result.exit_code == 0, result.output


def test_gse_remove_address_ptype():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "GSE-Remove-Destination-Address"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_set_dataset_non_existent():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-SET-DATASET-NON-EXISTENT"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_set_no_comm_smv_element():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-SET-NO-COMM"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_set_no_dst_addr_in_smv_element():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-SET-NO-DSTADDR"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_set_no_vlan_in_smv_element():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-SET-NO-VLAN"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_set_num_of_adsu_0():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-SET-NUM-ADSU-0"
            ]
        )
        assert result.exit_code == 0, result.output


def test_sv_set_num_of_adsu_1m():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/IOP_2019_HV_v9_ed2.0.scd")),
                "SV-SET-NUM-ADSU-1M"
            ]
        )
        assert result.exit_code == 0, result.output


def test_may_ignore():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/2021-08-27-iop-2007B-V0-R001.scd")),
                "MAY-IGNORE"
            ]
        )
        assert result.exit_code == 0, result.output


def test_ied_capabilities():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/2021-08-27-iop-2007B-V0-R001.scd")),
                "IED-CAPABILITY"
            ]
        )
        assert result.exit_code == 0, result.output

def test_must_understand():
    runner = CliRunner()
    original_working_directory = Path('.').resolve()
    with runner.isolated_filesystem():
        result = runner.invoke(
            cli,
            [
                "-f",
                str(original_working_directory / Path("tests/resources/2021-08-27-iop-2007B-V0-R001.scd")),
                "MUST-UNDERSTAND-R-GOOSE"
            ]
        )
        assert result.exit_code == 0, result.output
