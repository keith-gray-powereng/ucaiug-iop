Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
poetry run poetry env info
Write-Host "Running flake8"
poetry run flake8 --format=pylint --output-file pyflakes.log --tee $package_name 
if (!$?) { Exit $LASTEXITCODE }
