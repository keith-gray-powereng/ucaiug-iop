Set-PSDebug -Trace 1
Write-Host "CI_COMMIT_BRANCH: $CI_COMMIT_BRANCH"
Write-Host "CI_OPEN_MERGE_REQUESTS: $CI_OPEN_MERGE_REQUESTS"
Write-Host "CI_PIPELINE_SOURCE: $CI_PIPELINE_SOURCE"
Write-Host "Settings GIT_NO_SSL_VERIFY"
$env:GIT_SSL_NO_VERIFY = $true
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Poetry Version:"
poetry --version --ansi
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Environment Info"
poetry env info --ansi 
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Running python -V"
poetry run python -V
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Running poetry install"
poetry install --ansi -vvv
if (!$?) { Exit $LASTEXITCODE }