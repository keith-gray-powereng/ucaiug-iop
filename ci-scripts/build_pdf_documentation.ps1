Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
Write-Host "Building PDF Documentation"
poetry run sphinx-build -M latex docs docs\_build -W -a
if (!$?) { Exit $LASTEXITCODE }
poetry run pdflatex -aux-directory=docs/_build/latex -output-directory=docs/_build/latex "docs/_build/latex/ucaiugiopscltools.tex"
if (!$?) { Exit $LASTEXITCODE }
poetry run pdflatex -aux-directory=docs/_build/latex -output-directory=docs/_build/latex "docs/_build/latex/ucaiugiopscltools.tex"
if (!$?) { Exit $LASTEXITCODE }
