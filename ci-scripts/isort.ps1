Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
poetry run poetry env info
Write-Host "Running isort"
poetry run isort --check $package_name 
if (!$?) { Exit $LASTEXITCODE }
