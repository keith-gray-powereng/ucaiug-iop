Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
Write-Host "Publishing Release"
$env:pdfdocsjobid = $CI_JOB_ID - 1
$env:htmldocsjobid = $CI_JOB_ID - 2
$env:executable = "{`"name`":`"Executable`",`"url`":`"https://sas-gitlab.powereng.com/kgray/ucaiug-iop/-/jobs/artifacts/$CI_COMMIT_TAG/raw/dist/ucaiug-iop-$CI_COMMIT_TAG.exe?job=build_tag`",`"link_type`":`"package`"}"
$env:executablejson = $env:executable | ConvertTo-Json
Write-Host $env:executablejson
$env:pdfdocs = "{`"name`":`"PDF-Documentation`",`"url`":`"https://sas-gitlab.powereng.com/kgray/ucaiug-iop/-/jobs/artifacts/$CI_COMMIT_TAG/raw/docs/_build/latex/ucaiug-iop.pdf?job=build_pdf_documentation`",`"link_type`":`"package`"}"
$env:pdfdocsjson = $env:pdfdocs | ConvertTo-Json
Write-Host $env:pdfdocsjson
$env:htmldocs = "{`"name`":`"HTML-Documentation`",`"url`":`"https://sas-gitlab.powereng.com/kgray/ucaiug-iop/-/jobs/artifacts/$CI_COMMIT_TAG/raw/docs/_build/html.zip?job=build_html_documentation`",`"link_type`":`"package`"}"
$env:htmldocsjson = $env:htmldocs | ConvertTo-Json
Write-Host $env:htmldocsjson
$executable_hash = (Get-FileHash -Path "dist\ucaiug-iop-$CI_COMMIT_TAG.exe").Hash
$description = "Hash: $executable_hash"
release-cli create --name $CI_COMMIT_TAG --description "$description" --ref $CI_COMMIT_TAG --tag-name $CI_COMMIT_TAG --assets-link=$env:executablejson --assets-link=$env:pdfdocsjson --assets-link=$env:htmldocsjson
