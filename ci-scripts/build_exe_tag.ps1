Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
Write-Host "Building executable (tag)"
poetry run pyinstaller --onefile --name ucaiug-iop-$CI_COMMIT_TAG --copy-metadata ucaiug-iop --add-data "ucaiug_iop\resources;resources" ucaiug_iop\ucaiug_iop.py
if (!$?) { Exit $LASTEXITCODE }
