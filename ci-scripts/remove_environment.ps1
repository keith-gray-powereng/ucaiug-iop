Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
$environment_path = poetry env info --path
Write-Host "Removing Environment: $environment_path"
$environment_name = (Split-Path -Path $environment_path -Leaf)
Write-Host "Removing Environment: C:\users\gitlab-runner\appdata\local\pypoetry\cache\virtualenvs\$environment_name"
#poetry env remove (Split-Path -Path $environment_path -Leaf)
Remove-Item -Path "C:\users\gitlab-runner\appdata\local\pypoetry\cache\virtualenvs\$environment_name" -Force -Recurse
if (!$?) { Exit $LASTEXITCODE }