Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
poetry run poetry env info
Write-Host "Running coverage"
poetry run python -m pytest --junitxml=junit.xml --cov-report=html --cov=$package_name --cov-report=term-missing --cov-branch tests
if (!$?) { Exit $LASTEXITCODE }
