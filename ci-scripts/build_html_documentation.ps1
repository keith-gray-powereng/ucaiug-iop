Set-PSDebug -Trace 1
Write-Host "Package Name: $package_name"
Write-Host "Building HTML Documentation"
poetry run sphinx-build -M html docs docs\_build -W -a
if (!$?) { Exit $LASTEXITCODE }
Write-Host "Zipping the html docs directory"
Compress-Archive -path "docs\_build\html" "docs\_build\html.zip" -compressionlevel optimal
if (!$?) { Exit $LASTEXITCODE }
