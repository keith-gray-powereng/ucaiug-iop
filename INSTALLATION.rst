.. _section-heading-installation:

============
Installation
============

Stable release
--------------

The application is distributed as a single executable file.
Download the latest ucaiug-iop-yyyy.mm.dd.exe release from 
https://gitlab.com/keith-gray-powereng/ucaiug-iop/-/releases.

Open a command prompt and run the downloaded executable.
See the :ref:`section-heading-usage` section for more details on using the application.

Upgrade
-------

The application is distributed as as single executable file.
Therefore, an upgrade consists of downloading the latest 
ucaiug-iop-yyyy.mm.dd.exe release from 
https://gitlab.com/keith-gray-powereng/ucaiug-iop/-/releases
and running it instead of the older version
